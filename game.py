from random import randint
# vars to establish : guessNum(up to 5), name, guessMonth(1-12), guessYob(1900-2022)

guessNum=1
guessMonth = 1
guessYob = 1

name = input("Hi! What is your name? ")
while guessNum != 6:
    guessMonth = str(randint(1,12))
    guessYob = str(randint(1900,2022))
    print("Guess ", guessNum, " : "+ name + " were you born in " + guessMonth + " " + "/" + " " + guessYob + "?")
    response = input("\nyes or no? ")

    if response == "yes":
        print("\nI knew it!")
        break
    elif response == "later":
        print("\nDrat! Let me try again")
        guessNum = guessNum+1
    elif response == "earlier":
print("\nI have other things to do. Good Bye.")
